<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211101141515 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE characters ADD picture LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE movie_character DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE movie_character ADD PRIMARY KEY (character_id, movie_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE characters DROP picture');
        $this->addSql('ALTER TABLE movie_character DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE movie_character ADD PRIMARY KEY (movie_id, character_id)');
    }
}
