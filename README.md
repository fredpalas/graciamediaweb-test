# Start Wars Gracia media test

Application Made with Symfony, VueJs, Bootstrap

## Installation 

### Requirements

[Docker](https://docs.docker.com/engine/install/)

[Docker compose](https://docs.docker.com/compose/install/)

### Instruction 
Open a terminal and execute
```bash
./first_start_from_host.sh
```
 
This command will build a Docker image for PHP and pull images for MySQL and NGINX.

The nginx is running on port 1000

## Usage

### Normal Access
Access to Characters

http://localhost:1000/

Access to movies

http://localhost:1000/movies
