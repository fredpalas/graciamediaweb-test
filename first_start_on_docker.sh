# bin/bash
composer install
bin/console doctrine:database:drop -n --force
bin/console doctrine:database:create -n
bin/console doctrine:migrations:migrate -n
bin/console starwars:import
bash build.sh