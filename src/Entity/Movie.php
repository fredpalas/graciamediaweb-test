<?php

namespace App\Entity;

use App\Repository\MovieRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity(repositoryClass=MovieRepository::class)
 * @ORM\Table("movies")
 */
class Movie
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("movie","characters")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("movie","characters")
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Character", mappedBy="movies", cascade={"persist"})
     * @ORM\JoinTable(name="movie_character")
     * @MaxDepth(1)
     * @Groups("movie")
     */
    private $characters;

    public function __construct()
    {
        $this->characters = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Character[]
     */
    public function getCharacters(): Collection
    {
        return $this->characters;
    }

    public function addCharacter(Character $character): self
    {
        if (!$this->characters->contains($character)) {
            $this->characters[] = $character;
        }

        return $this;
    }

    public function removeCharacter(Character $character): self
    {
        $this->characters->removeElement($character);

        return $this;
    }

    /**
     * @param ArrayCollection $characters
     * @return Movie
     */
    public function setCharacters(ArrayCollection $characters): Movie
    {
        $this->characters = $characters;

        return $this;
    }
}
