<?php

namespace App\Entity;

use App\Repository\CharacterRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity(repositoryClass=CharacterRepository::class)
 * @ORM\Table("characters")
 */
class Character
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("movie","characters")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("movie","characters")
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     * @Groups("movie","characters")
     */
    private $mass;

    /**
     * @ORM\Column(type="integer")
     * @Groups("movie","characters")
     */
    private $height;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups("movie","characters")
     */
    private $gender;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Movie", inversedBy="characters", cascade={"persist"})
     * @ORM\JoinTable(name="movie_character")
     * @MaxDepth(1)
     * @Groups("characters")
     */
    private $movies;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups("movie","characters")
     */
    private $picture;

    public function __construct()
    {
        $this->movies = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMass(): ?int
    {
        return $this->mass;
    }

    public function setMass(int $mass): self
    {
        $this->mass = $mass;

        return $this;
    }

    public function getHeight(): ?int
    {
        return $this->height;
    }

    public function setHeight(int $height): self
    {
        $this->height = $height;

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * @return Collection|Movie[]
     */
    public function getMovies(): Collection
    {
        return $this->movies;
    }

    public function addMovie(Movie $movie): self
    {
        if (!$this->movies->contains($movie)) {
            $this->movies[] = $movie;
            $movie->addCharacter($this);
        }

        return $this;
    }

    public function removeMovie(Movie $movie): self
    {
        if ($this->movies->removeElement($movie)) {
            $movie->removeCharacter($this);
        }

        return $this;
    }

    /**
     * @param ArrayCollection $movies
     * @return Character
     */
    public function setMovies(ArrayCollection $movies): Character
    {
        $this->movies = $movies;

        return $this;
    }

    public function getPicture(): ?string
    {
        return $this->picture;
    }

    public function setPicture(?string $picture): self
    {
        $this->picture = $picture;

        return $this;
    }
}
