<?php


namespace App\Controller;


use App\Form\CharacterType;
use App\Form\ProjectType;
use App\Repository\CharacterRepository;
use App\Service\CreateUpdateService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/api/characters")
 */
class CharacterController extends AbstractController
{
    /**
     * @var CharacterRepository
     */
    private $characterRepository;
    /**
     * @var SerializerInterface
     */
    private $serializer;
    /**
     * @var CreateUpdateService
     */
    private $createUpdateService;

    public function __construct(CharacterRepository $characterRepository, SerializerInterface $serializer, CreateUpdateService $createUpdateService)
    {

        $this->characterRepository = $characterRepository;
        $this->serializer = $serializer;
        $this->createUpdateService = $createUpdateService;
    }

    /**
     * @Route(methods={"get"})
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $name = $request->query->get('name', null);

        $results = $this->characterRepository->findByName($name);

        $data = $this->serializer->serialize($results, JsonEncoder::FORMAT,
            ['groups' => 'characters', AbstractObjectNormalizer::ENABLE_MAX_DEPTH => true]);

        return new JsonResponse($data, 200, [], true);
    }

    /**
     * @Route("/{id}", methods={"POST"})
     * @param $id
     * @return JsonResponse
     */
    public function editCharacter($id)
    {
        $entity = $this->characterRepository->find($id);

        $form = $this->createUpdateService->createWithEntity(CharacterType::class, $entity);
        if ($form && $form instanceof FormInterface) {
            return new JsonResponse([
              'error' =>  (string)$form->getErrors(true)
            ], 403);
        }

        $data = $this->serializer->serialize($entity, JsonEncoder::FORMAT,
            ['groups' => 'characters', AbstractObjectNormalizer::ENABLE_MAX_DEPTH => true]);

        return new JsonResponse($data, 200, [], true);
    }

    /**
     * @Route("/{id}", methods={"DELETE"})
     * @param $id
     * @return JsonResponse
     */
    public function deleteCharacter($id)
    {
        $entity = $this->characterRepository->find($id);
        $this->createUpdateService->deleteEntity($entity);

        return new JsonResponse(null, 202);
    }
}