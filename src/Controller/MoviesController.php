<?php


namespace App\Controller;


use App\Repository\MovieRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class CharacterControllers
 * @package App\Controller
 * @Route("/api/movies")
 */
class MoviesController extends AbstractController
{
    /**
     * @var MovieRepository
     */
    private $movieRepository;
    /**
     * @var SerializerInterface
     */
    private $serializer;

    public function __construct(MovieRepository $movieRepository, SerializerInterface $serializer) {

        $this->movieRepository = $movieRepository;
        $this->serializer = $serializer;
    }

    /**
     * @Route(methods={"get"})
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $name = $request->query->get('name', null);

        $results = $this->movieRepository->findByName($name);

        $data = $this->serializer->serialize($results, JsonEncoder::FORMAT,
            ['groups' => 'characters', AbstractObjectNormalizer::ENABLE_MAX_DEPTH => true]);

        return new JsonResponse($data, 200, [], true);
    }
}