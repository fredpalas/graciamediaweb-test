<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    /**
     * @Route(methods={"get"})
     */
    public function index()
    {
        return new Response($this->renderView('characters.html.twig'));
    }

    /**
     * @Route("movies",methods={"get"})
     */
    public function movies()
    {
        return new Response($this->renderView('movies.html.twig'));
    }
}