<?php


namespace App\Service;


use Exception;
use GuzzleHttp\ClientInterface;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

class SwapiHttpClient
{

    const CHARACTERS = '/api/people?page=%s';
    const MOVIES = '/api/films';

    /**
     * @var ClientInterface
     */
    private $client;

    public function __construct(ClientInterface $client) {

        $this->client = $client;
    }

    /**
     * @param int $number
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getCharacters(int $number)
    {
        if ($number <= 0) {
            throw new Exception('Number can not be smaller or equal to 0');
        }
        $pages = $number / 10 + ($number % 10 > 0 ? 1 : 0);
        $range = range(1, $pages);
        $characters = [];
        foreach ($range as $page) {
            $url = sprintf(self::CHARACTERS, $page);
            $responseData = $this->getCollection($url);
            foreach ($responseData['results'] as $response) {
                $characters[] = [
                    'name' => $response['name'],
                    'height' => (int) $response['height'],
                    'mass' => (int) $response['mass'],
                    'gender' => $response['gender'],
                    'movies' => $this->parseMovies($response['films'])
                ];
            }

        }

        return $characters;
    }

    /**
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getmovies()
    {
        $response = $this->getCollection(self::MOVIES);
        $films = [];
        foreach ($response['results'] as $film)
        {
            $films[] = [
                'id' => $this->parseMovie($film['url']),
                'name' => $film['title']
            ];
        }

        return $films;
    }

    /**
     * @param array $movies
     * @return array
     * @throws Exception
     */
    private function parseMovies(array $movies) {
        $response = [];
        foreach ($movies as $movie) {
            $response[] = $this->parseMovie($movie);
        }

        return $response;
    }

    private function parseMovie($url) {
        preg_match_all('/films\/([\d]+)/', $url,$matches);

        if (count($matches) != 2) {
            throw new Exception(sprintf('Url %s don\'t have a valid movie url', $url));
        }
        return (int)$matches[1][0];
    }

    /**
     * @param $url
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws Exception
     */
    private function getCollection($url)
    {
        $headers = [
            'Accept' => 'application/json'
        ];
        $response = $this->client->request('get', $url, $headers);
        $this->checkStatusCode($response);
        return json_decode((string) $response->getBody(), true);
    }

    /**
     * @param ResponseInterface $response
     * @throws Exception
     */
    private function checkStatusCode(ResponseInterface $response)
    {
        $dataString = (string)$response->getBody();
        $dataJson = json_decode($dataString, true);
        $message = $dataJson['message'] ?? null;
        switch ($code = $response->getStatusCode()) {
            case $code >= 200 && $code <= 208;
                break;
            case 401:
                throw new AuthenticationException($message);
            case 404:
                throw new NotFoundHttpException($message);
            case 403:
                throw new HttpException(403, $message);
            case 422:
                throw new UnprocessableEntityHttpException($message);
            case 500:
            default;
                throw new Exception($message);
        }
    }
}