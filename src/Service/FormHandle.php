<?php


namespace App\Service;


use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class FormHandle
{

    /**
     * @var RequestStack
     */
    private $requestStack;
    /**
     * @var Request | null
     */
    private $request;
    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    public function __construct(RequestStack $requestStack, FormFactoryInterface $formFactory)
    {
        $this->requestStack = $requestStack;
        $this->formFactory = $formFactory;
    }


    /**
     * @param string $formClass
     * @param $entity
     * @return bool|\Symfony\Component\Form\FormInterface
     */
    public function createFormHandle(string $formClass, $entity)
    {
        $options = [];
        if ($this->getRequest()->getMethod() == 'PUT') {
            $options = ['method' => 'PUT'];
        }
        if ($this->getRequest()->getMethod() == 'POST') {
            $options = ['method' => 'POST'];
        }


        $form = $this->createForm($formClass, $entity, $options);
        $form->handleRequest($this->getRequest());
        dump($formClass);
        if (($form->isSubmitted() && !$form->isValid()) || !$form->isSubmitted()) {
            $form->getErrors();
            return $form;
        }

        return false;
    }

    /**
     * @return Request|null
     */
    protected function getRequest()
    {
        return $this->request ?? $this->request = $this->requestStack->getCurrentRequest();
    }

    /**
     * @param $formClass
     * @param $entity
     * @param array $options
     * @return \Symfony\Component\Form\FormInterface
     */
    protected function createForm($formClass, $entity, array $options = [])
    {
        return $this->formFactory->create($formClass, $entity, $options);
    }
}