<?php


namespace App\Service;


use App\Entity\Character;
use App\Entity\Movie;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Output\Output;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizableInterface;
use Symfony\Component\Serializer\SerializerInterface;

class StarWarsImport
{
    /**
     * @var SwapiHttpClient
     */
    private $swapiHttpClient;
    /**
     * @var OutputInterface | null
     */
    private $output = null;
    /**
     * @var DenormalizerInterface
     */
    private $denormalizer;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(
        SwapiHttpClient $swapiHttpClient,
        DenormalizerInterface $denormalizer,
        EntityManagerInterface $entityManager
    ) {

        $this->swapiHttpClient = $swapiHttpClient;
        $this->denormalizer = $denormalizer;
        $this->entityManager = $entityManager;
    }

    /**
     * @param OutputInterface $output
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function importDataFromConsole(OutputInterface $output)
    {
        $this->output = $output;
        $this->importData();
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function importData()
    {
        if ($this->output) {
            $this->output->writeln('Importing Movies');
        }
        $movies = $this->swapiHttpClient->getmovies();
        $moviesObj = [];
        foreach ($movies as $movie) {
            $movieObj = $this->denormalizer->denormalize($movie, Movie::class);
            $moviesObj[$movie['id']] = $movieObj;
            if ($this->output) {
                $this->output->writeln(sprintf('Importing Movie: %s', $movie['name']));
            }
            $this->entityManager->persist($movieObj);
        }
        $this->entityManager->flush();
        if ($this->output) {
            $this->output->writeln('Importing Characters');
        }
        $characters = $this->swapiHttpClient->getCharacters(30);

        foreach ($characters as $character) {
            $moviesChar = $character['movies'];
            unset($character['movies']);
            /** @var Character $char */
            $char = $this->denormalizer->denormalize($character, Character::class);
            $this->entityManager->persist($char);
            $char->setMovies($this->getMovies($moviesChar, $moviesObj));
            if ($this->output) {
                $this->output->writeln(sprintf('Importing Character: %s', $character['name']));
            }
            $this->entityManager->persist($char);
        }

        $this->entityManager->flush();
    }

    private function getMovies(array $ids, array $movies)
    {
        $arrayCollection = new ArrayCollection();
        foreach ($ids as $id)
        {
            $arrayCollection->add($movies[$id]);
        }

        return $arrayCollection;
    }
}