<?php


namespace App\Service;


use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormInterface;

class CreateUpdateService
{
    /**
     * @var FormHandle
     */
    private $formHandle;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    private $entity;

    public function __construct(FormHandle $formHandle, EntityManagerInterface $entityManager)
    {
        $this->formHandle = $formHandle;
        $this->entityManager = $entityManager;
    }

    /**
     * @param string $formClass
     * @param string $entityClass
     * @return bool|FormInterface
     */
    public function create(string $formClass, string $entityClass)
    {
        $entity = new $entityClass;

        return $this->createWithEntity($formClass, $entity);
    }

    /**
     * @param string $formClass
     * @param object $entity
     * @return bool|FormInterface
     */
    public function createWithEntity(string $formClass, object $entity)
    {
        $this->entity = $entity;
        $form = $this->form($formClass, $entity);

        if ($form && $form instanceof FormInterface) {
            return $form;
        }

        return false;
    }

    public function deleteEntity($entity)
    {
        $this->entityManager->remove($entity);
        $this->entityManager->flush();
    }

    protected function form(string $formClass, $entity)
    {
        $form = $this->formHandle->createFormHandle($formClass, $entity);

        if ($form && $form instanceof FormInterface) {
            return $form;
        }

        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        return false;
    }

    public function getEntity()
    {
        return $this->entity;
    }
}