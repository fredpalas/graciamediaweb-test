<?php

namespace App\Command;

use App\Service\StarWarsImport;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class StarwarsImportCommand extends Command
{
    protected static $defaultName = 'starwars:import';
    protected static $defaultDescription = 'Import 30 characters and The 6 first movies';
    /**
     * @var StarWarsImport
     */
    private $starWarsImport;

    public function __construct(string $name = null, StarWarsImport $starWarsImport) {
        parent::__construct($name);
        $this->starWarsImport = $starWarsImport;
    }

    protected function configure(): void
    {
        $this
            ->setDescription(self::$defaultDescription)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $io->writeln('Start importing');

        $this->starWarsImport->importDataFromConsole($output);

        return 0;
    }
}
